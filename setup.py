#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='shared',
      version='0.0.1',
      description='Save Rudy.',
      author='S.J. Myers',
      author_email='smyers@ou.edu',
      packages=find_packages(exclude=[]),
      license='LICENSE.txt',
    )
